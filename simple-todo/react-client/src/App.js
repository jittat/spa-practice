import React, { Component } from 'react';
import './App.css';

function TodoItem(props) {
  let updateTextInput = '';
  if (! props.item.updatePrep) {
    updateTextInput = '';
  } else {
    updateTextInput = (
      <span>
        <input 
          type="text" 
          value={props.item.updatedTask}
          onChange={props.onUpdateTaskChange}
        />
        &nbsp;
      </span>
    );
  }
  return (
    <li key={props.item.id}>
      {props.item.task} &nbsp;
      {updateTextInput}
      <button onClick={props.onUpdateClick}>Update</button> &nbsp;
      <button onClick={props.onDeleteClick}>Delete</button>
    </li>
  );
}

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      todoItems: [],
      newTodoTask: '',
      message: '',
    };
  }

  fetchTodoItems() {
    fetch('http://localhost:5000/items')
      .then(response => response.json()) 
      .then(data => {
        this.setState({ 
          todoItems: data.items.map((item) => ({
            ...item,
            updatePrep: false,
            updatedTask: '',
          })), 
        });
      });
  }

  postNewItem(task, dataHandler) {
    const data = {
      task: task,
    };
    fetch('http://localhost:5000/items', {
      method: 'POST',
      mode: "cors",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then((res) => res.json())
      .then((data) => {
        dataHandler(data);
      });
  }

  postUpdateItem(id, updatedTask, dataHandler) {
    const data = {
      task: updatedTask,
    };
    fetch(`http://localhost:5000/items/${id}`, {
      method: 'PUT',
      mode: "cors",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data),
    }).then((res) => res.json())
      .then((data) => {
        dataHandler(data);
      });
  }

  deleteItem(id, dataHandler) {
    fetch(`http://localhost:5000/items/${id}`, {
      method: 'DELETE',
      mode: "cors",
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
    }).then((res) => res.json())
      .then((data) => {
        dataHandler(data);
      });
  }

  componentDidMount() {
    this.fetchTodoItems();
  }

  render() {
    const todoList = this.state.todoItems.map((item, index) => {
      return (
        <TodoItem 
          key={item.id} 
          item={item}
          onUpdateClick={() => this.handleUpdateClick(item, index)}
          onDeleteClick={() => this.handleDeleteClick(item, index)}
          onUpdateTaskChange={(event) => this.handleUpdateTaskChange(event, item, index)} 
        />
      );
    });
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Todo List</h1>
        </header>
        <div>
          <input type="text" value={this.state.newTodoTask}
            onChange={this.handleNewTaskChange.bind(this)} />
          &nbsp;
          <button onClick={this.handleCreateTaskClick.bind(this)}>Create</button>
        </div>
        <div>
          {this.state.message !== '' ? this.state.message : ''}
        </div>
        <ul>{todoList}</ul>
      </div>
    );
  }

  handleNewTaskChange(event) {
    this.setState({ newTodoTask: event.target.value });
  }

  handleCreateTaskClick(event) {
    event.preventDefault();

    this.postNewItem(this.state.newTodoTask,
      (data) => {
        this.setState({ 
          message: data.message,
          newTodoTask: '',
        });
        this.fetchTodoItems();
      });
  }

  handleUpdateTaskChange(event, item, index) {
    const items = this.state.todoItems.slice();
    items[index].updatedTask = event.target.value;
    this.setState({ todoItems: items });
  }

  handleUpdateClick(item, index) {
    if (! item.updatePrep) {
      const items = this.state.todoItems.slice();
      items[index].updatePrep = true;
      this.setState({ todoItems: items });
    } else {
      this.postUpdateItem(item.id, item.updatedTask,
        (data) => {
          this.setState({ 
            message: data.message,
          });
          this.fetchTodoItems();
        });
    }
  }

  handleDeleteClick(item, index) {
    this.deleteItem(item.id, 
      (data) => {
        this.setState({ 
          message: data.message,
        });
        this.fetchTodoItems();
      });
  }
}

export default App;
