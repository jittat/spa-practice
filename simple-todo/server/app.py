from flask import Flask, jsonify, request
from flask_cors import CORS
import uuid

DEBUG = True

app = Flask(__name__)
app.config.from_object(__name__)
CORS(app)

todo_items = [
    {
        'id': uuid.uuid4().hex,
        'task': 'Sleep',
    },
    {
        'id': uuid.uuid4().hex,
        'task': 'Eat',
    },
]

@app.route('/items', methods=['GET'])
def all_items():
    return jsonify({
        'status': 'success',
        'items': todo_items,
    })

@app.route('/items', methods=['POST'])
def add_new_item():
    post_data = request.get_json()
    todo_items.append({
        'id': uuid.uuid4().hex,
        'task': post_data.get('task')
    })
    return jsonify({
        'status': 'success',
        'message': 'Todo item created',
    })

def remove_item(item_id):
    for item in todo_items:
        if item['id'] == item_id:
            todo_items.remove(item)
            return True
    return False

@app.route('/items/<item_id>', methods=['PUT', 'DELETE'])
def single_item(item_id):
    response_object = {'status': 'success'}
    if request.method == 'PUT':
        post_data = request.get_json()
        remove_item(item_id)
        todo_items.append({
            'id': uuid.uuid4().hex,
            'task': post_data.get('task')
        })
        response_object['message'] = 'Item updated'
    elif request.method == 'DELETE':
        if remove_item(item_id):
            response_object['message'] = 'Item deleted'
        else:
            response_object['message'] = 'Item not found'
    return jsonify(response_object)
    
if __name__ == '__main__':
    app.run()
