import Vue from 'vue';
import Router from 'vue-router';
import TodoItems from '@/components/TodoItems';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TodoItems',
      component: TodoItems,
    },
  ],
  mode: 'hash',
});
