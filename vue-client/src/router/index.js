import Vue from 'vue';
import Router from 'vue-router';
import TodoItems from '@/components/TodoItems';
import Login from '@/components/Login';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'TodoItems',
      component: TodoItems,
      beforeEnter (to, from, next) {
        if (localStorage.getItem('jwt') == null) {
          next('/login');
        } else {
          next();
        }
      },
    }, {
      path: '/login',
      name: 'Login',
      component: Login,
    },
  ],
  mode: 'hash',
});
